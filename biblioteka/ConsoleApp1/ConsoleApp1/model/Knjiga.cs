﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Knjiga
    {
        public int Id { get; set; }
        public string Ime { get; set; }
        public string Autor { get; set; }
        public int Godina { get; set; }
        public Clan Clan { get; set; }

        public Knjiga()
        {

        }

        public Knjiga(int id, string ime, string autor, int godina)
        {
            this.Id = id;
            this.Ime = ime;
            this.Autor = autor;
            this.Godina = godina;

        }
        public Knjiga(int id, string ime, string autor, int godina, Clan clan)
        {
            this.Id = id;
            this.Ime = ime;
            this.Autor = autor;
            this.Godina = godina;
            this.Clan = clan;
        }


        public override string ToString()
        {
            StringBuilder sb = new StringBuilder("Knjiga [" + Id + " " + Ime + " " + Autor + " " + Godina + "]");
            if (Clan != null)
                sb.Append(" " + Clan.Ime);
            return sb.ToString();
            
        }
    }
}
