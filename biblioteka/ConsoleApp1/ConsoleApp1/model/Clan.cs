﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Clan
    {
        public int Id { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }


        public Clan()
        {

        }
        public Clan(string ime, string prezime)
        {
            this.Ime = ime;
            this.Prezime = prezime;
        }

        public Clan(int id, string ime, string prezime)
        {
            this.Id = id;
            this.Ime = ime;
            this.Prezime = prezime;
  

        }
        public override string ToString()
        {
            string s = "Clan [" + Id + " " + Ime + " " + Prezime +  "]";         
            return s;
        }
    }
}
