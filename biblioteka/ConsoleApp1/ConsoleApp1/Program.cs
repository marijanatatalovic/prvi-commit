﻿using ConsoleApp1.ui;
using ConsoleApp1.utils;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        public static SqlConnection conn;

         static void LoadConnection()
        {
            try
            {
                string konekcija = "Data Source=.\\SQLEXPRESS;Initial Catalog=biblioteka;Integrated Security=True;MultipleActiveResultSets=True";
                conn = new SqlConnection(konekcija);
                conn.Open();
            }
            catch (Exception e)
            {

                Console.WriteLine(e.ToString()); ;
            }
        }

        public static void IspisiMenu()
        {
            Console.WriteLine("Studentska Sluzba - Osnovne opcije:");
            Console.WriteLine("\tOpcija broj 1 - Rad sa clanovima");
            Console.WriteLine("\tOpcija broj 2 - Rad sa knjigama");
            Console.WriteLine("\tOpcija broj 3 - Rad sa pohadjanjem predmeta");
            Console.WriteLine("\t\t ...");
            Console.WriteLine("\tOpcija broj 0 - IZLAZ IZ PROGRAMA");
        }
        static void Main(string[] args)
        {
            LoadConnection();
            int odluka = -1;
            while (odluka!=0)
            {
                IspisiMenu();
                Console.WriteLine("Unesite opciju");
                odluka = IO.OcitajCeoBroj();
                switch (odluka)
                {
                    case 0:
                        Console.WriteLine("Izlaz iz programa");
                        break;
                    case 1:
                       ClanUI.Meni();
                        break;
                    case 2:
                       KnjigaUI.Meni();
                        break;                 
                       
                    default:
                        Console.WriteLine("Nepostojeca komanda");
                        break;
                }
            

            }

        }
    }
}
