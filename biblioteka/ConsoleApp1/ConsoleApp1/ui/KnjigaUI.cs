﻿using ConsoleApp1.dao;
using ConsoleApp1.utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.ui
{
    class KnjigaUI
    {
        public static void IspisiMeni()
        {
            Console.WriteLine("Rad sa knjigama u biblioteci");           
            Console.WriteLine("\tOpcija broj 1-Ispisi sve knjige ");
            Console.WriteLine("\tOpcija broj 2-Unesi novu knjigu");
            Console.WriteLine("\tOpcija broj 3-Izmeni  knjigu");
            Console.WriteLine("\tOpcija broj 4-Obrisi knjigu");
            Console.WriteLine("\tOpcija broj 5-Pronadji clana kod koga je izdata knjiga");
            Console.WriteLine("\t\t.........");
            Console.WriteLine("\tOpcija broj 0- Povratak");

        }

        public static void IspisiSveKnjige()
        {
            List<Knjiga> sveKnjige = KnjigaDAO.GetAll(Program.conn);
            for (int i = 0; i < sveKnjige.Count; i++)
            {
                Console.WriteLine(sveKnjige[i]);
            }

        }

        public static Knjiga PronadjiKnjigu()
        {
            Knjiga knjiga = null;
            Console.WriteLine("unesite id knjige ");
            int id=IO.OcitajCeoBroj();
            knjiga = PronadjiKnjiguPoId(id);
            if (knjiga == null)
            {
                Console.WriteLine("Ne postoji knjiga sa tim id-jem");

            }
            return knjiga;


        }

        public static Knjiga PronadjiKnjiguPoId(int id)
        {
            Knjiga retVal = KnjigaDAO.GetKnjigaById(Program.conn, id);
            return retVal;

        }
        public static void UnesiNovuKnjigu()
        {
            Console.WriteLine("Unesite ime knjige");
            string ime = IO.OcitajTekst();
            Console.WriteLine("Unesite autora knjige");
            string autor = IO.OcitajTekst();
            Console.WriteLine("Unesite godinu izdanja");
            int godina = IO.OcitajCeoBroj();
            Console.WriteLine("Unesite id clana koji je uzeo knjigu");
            int idClana = IO.OcitajCeoBroj();

            Clan c= ClanUI.PronadjiClanaPoId(idClana);
            if (c == null)
            {
                c=ClanUI.UnosNovogClana();
            }

            Knjiga k = new Knjiga(0, ime, autor, godina, c);

            if(KnjigaDAO.PutNewBook(Program.conn, k))
            {
                Console.WriteLine("Knjiga je uspesno uneta");
            }else
                Console.WriteLine("Doslo je do greske.");
            
        }
        public static void IzmeniKnjigu()
        {
            Knjiga knjiga = KnjigaUI.PronadjiKnjigu();
            if (knjiga != null)
            {
                Console.WriteLine("Unesite novo ime knjige");
                string ime=IO.OcitajTekst();
                knjiga.Ime = ime;
                Console.WriteLine("Unesite novog autora knjige");
                string autor = IO.OcitajTekst();
                knjiga.Autor = autor;
                Console.WriteLine("Unesite novu godinu izdanja ");
                int godina = IO.OcitajCeoBroj();
                knjiga.Godina = godina;
                
                //  string ime = IO.OcitajTekst();
                Console.WriteLine("Unesite id clana koji je uzeo knjigu");
                int idClana = IO.OcitajCeoBroj();
                Clan c = ClanUI.PronadjiClanaPoId(idClana);
                if (c == null)
                {
                    c = ClanUI.UnosNovogClana();
                }
                knjiga.Clan.Id =c.Id;
                
                KnjigaDAO.Update(Program.conn,knjiga);
            }

        }
        public static void ObrisiKnjigu()
        {
            Knjiga knjiga = KnjigaUI.PronadjiKnjigu();
            if (knjiga !=null)
            {
                KnjigaDAO.Delete(Program.conn, knjiga.Id);
            }

        }
        public static void Meni()
        {
            int odluka = -1;
            while (odluka != 0)
            {
                IspisiMeni();
                Console.WriteLine("opcija :");
                odluka = IO.OcitajCeoBroj();
                switch (odluka)
                {
                    case 0:
                        Console.WriteLine("Izlaz iz programa");
                        break;
                    case 1:
                        IspisiSveKnjige();
                        break;
                    case 2:
                        UnesiNovuKnjigu();
                        break;
                    case 3:
                        IzmeniKnjigu();
                        break;
                    case 4:
                        ObrisiKnjigu();
                        break;
                    case 5:
                        //PronadjiClanaKodKogaJeIzdataKnjiga();
                        break;
                    default:
                        Console.WriteLine("Nepostojeca opcija:");
                        break;
                }

            }
        }

    }
}
