﻿using ConsoleApp1.dao;
using ConsoleApp1.utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.ui
{
    class ClanUI
    {

        public static void ispisiMeni()
        {
            Console.WriteLine("Rad sa studentima");
            Console.WriteLine("\tOpcija broj 1- ispis svih clanova");
            Console.WriteLine("\tOpcija broj 2- unos novog clana");
            Console.WriteLine("\tOpcija broj 3-izmena clana");
            Console.WriteLine("\tOpcija broj 4-brisanje clana");
            Console.WriteLine("\t\t...");
            Console.WriteLine("\tOpcija broj 0...Nazad");

        }
        public static void IspisiSveClanove()
        {
            List<Clan> sviClanovi = ClanDAO.GetAll(Program.conn);
            for (int i = 0; i < sviClanovi.Count; i++)
            {
                Console.WriteLine(sviClanovi[i]);

            }
        }

        public static Clan PronadjiClanaPoId()
        {
            Clan retVal = null;
            Console.WriteLine("Unesite id clana");
            int id = IO.OcitajCeoBroj();
            retVal = PronadjiClanaPoId(id);
            if (retVal == null)
                Console.WriteLine("Clan sa tim id-jem " + id
                         + " ne postoji u evidenciji");
            return retVal;

        }

        public static Clan PronadjiClanaPoId(int id)
        {
            Clan retVal = ClanDAO.GetClanById(Program.conn,id);
            return retVal;

        }
        public static Clan UnosNovogClana()
        {
            
            Console.Write("Unesi ime:");
            string stIme = IO.OcitajTekst();
            Console.Write("Unesi prezime:");
            string stPrezime = IO.OcitajTekst();
            

            Clan st = new Clan(0,stIme, stPrezime);
            // Ovde se moze proveravati i povratna vrednost i onda ispisivati poruka
            ClanDAO.Add(Program.conn, st);
            return st;

        }

        public static void IzmenaClana()
        {
            Clan c = PronadjiClanaPoId();
            if (c != null)
            {
                Console.Write("Unesi ime:");
                string stIme = IO.OcitajTekst();
                c.Ime = stIme;
                Console.Write("Unesi prezime:");
                string stPrezime = IO.OcitajTekst();
                c.Prezime = stPrezime;
            }
            

            ClanDAO.Update(Program.conn,c);


        }

        public static void BrisanjeClana()
        {
            Clan c = PronadjiClanaPoId();
            if (c!=null)
            {
                ClanDAO.Delete(Program.conn, c.Id);
            }
        }
        public static void Meni()
        {
            int odluka=-1;
            while (odluka != 0)
            {
                ispisiMeni();
                Console.WriteLine("Unesite opciju ");
                odluka = IO.OcitajCeoBroj();
                switch (odluka)
                {
                    case 0:
                        Console.WriteLine("Izlaz");
                        break;
                    case 1:
                        IspisiSveClanove();
                        break;
                    case 2:
                        UnosNovogClana();
                        break;
                    case 3:
                        IzmenaClana();
                        break;
                    case 4:
                        BrisanjeClana();
                        break;
                    default:
                        Console.WriteLine("Nepostojeca komanda");
                        break;


                }


            }
        }


    }
}
