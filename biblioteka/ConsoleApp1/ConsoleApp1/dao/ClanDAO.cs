﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.dao
{
    class ClanDAO
    {
        public static Clan  GetClanById(SqlConnection conn, int id)
        {
            Clan clan = null;
            try
            {
                string query = " select ime, prezima from clan where id=" + id;
                SqlCommand cmd = new SqlCommand(query, conn);
                SqlDataReader rdr = cmd.ExecuteReader();

                if (rdr.Read())
                {
                    
                    string ime = (string)rdr["ime"];
                    string prezime = (string)rdr["prezima"];
                    clan = new Clan(id,ime, prezime);

                }
                rdr.Close();
            }
            catch (Exception e)
            {

                Console.WriteLine(e.ToString()) ;
            }
            return clan;

        }

        public static bool Add(SqlConnection conn, Clan clan)
        {
            bool retVal = false;
            try
            {
                string update = "insert into clan(ime, prezima) values (@ime, @prezime)";
                SqlCommand cmd = new SqlCommand(update, conn);

                cmd.Parameters.AddWithValue("@ime",clan.Ime );
                cmd.Parameters.AddWithValue("@prezime", clan.Prezime);
                if (cmd.ExecuteNonQuery() == 1)
                {
                    retVal = true;
                }
            }
            catch (Exception e)
            {

                Console.WriteLine(e.ToString()); ;
            }
            return retVal;
        }

        public static List<Clan> GetAll(SqlConnection conn)
        {
            List<Clan> retVall = new List<Clan>();
            try
            {
                string query = "Select id, ime,prezima from Clan";
                SqlCommand cmd = new SqlCommand(query, conn);
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    int id = (int)rdr["id"];
                    string ime=(string)rdr["ime"];
                    string prezime = (string)rdr["prezima"];
                    Clan clan = new Clan(id, ime, prezime);
                    retVall.Add(clan);
                }
                rdr.Close();
            }
            catch (Exception e)
            {

                Console.WriteLine(e.ToString()); 
            }
            return retVall;
        }

        public static bool Update(SqlConnection con, Clan clan)
        {
            bool retVal = false;
            try
            {
                string update = "UPDATE clan SET ime=@ime, prezima=@prezime WHERE id=@clanid";
                SqlCommand cmd = new SqlCommand(update, con);
                cmd.Parameters.AddWithValue("@ime", clan.Ime);
                cmd.Parameters.AddWithValue("@prezime", clan.Prezime);
                cmd.Parameters.AddWithValue("@clanid", clan.Id);
                if (cmd.ExecuteNonQuery() == 1)
                    retVal = true;
            }
            catch (Exception e)
            {

                Console.WriteLine(e.ToString()); ;
            }
            return retVal;
        }

        public static bool Delete(SqlConnection conn, int id)
        {
            bool retVal = false;
            try
            {
                string update = "DELETE FROM clan WHERE id = " + id;
                SqlCommand cmd = new SqlCommand(update, conn);

                if (cmd.ExecuteNonQuery() == 1)
                    retVal = true;

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return retVal;
        }
    }
}

