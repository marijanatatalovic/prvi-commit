﻿using ConsoleApp1.ui;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.dao
{
    class KnjigaDAO
    {
        public static bool Delete(SqlConnection con, int id)
        {
        


            bool retVal = false;
            try
            {

                string del = "Delete from knjiga where id=@id " ;
                SqlCommand cmd = new SqlCommand(del, con);
                cmd.Parameters.AddWithValue("@id", id);

                if (cmd.ExecuteNonQuery() == 1)
                {
                    retVal= true;
                }
            }
            catch (Exception e)
            {

                Console.WriteLine(e.ToString());
            }
            return retVal;
        }
        public static bool Update(SqlConnection con, Knjiga knjiga)
        {
            bool retVal = false;
            try
            {
                string update = "Update knjiga set ime=@ime, autor=@autor, godina_izdanja=@godina_izdanja, clan_id=@clan_id where id= @knjiga_id";
                SqlCommand cmd = new SqlCommand(update, con);
                cmd.Parameters.AddWithValue("@ime", knjiga.Ime);
                cmd.Parameters.AddWithValue("@autor", knjiga.Autor);
                cmd.Parameters.AddWithValue("@godina_izdanja", knjiga.Godina);
                cmd.Parameters.AddWithValue("@clan_id", knjiga.Clan.Id);
                cmd.Parameters.AddWithValue("@knjiga_id", knjiga.Id);
                if (cmd.ExecuteNonQuery() == 1)
                {
                    retVal = true;
                }

            }
            catch (Exception e)
            {

                Console.WriteLine(e.ToString()); ;
            }
            return retVal;
        }
        
        public static Knjiga GetKnjigaById(SqlConnection conn, int id)
        {
            Knjiga knjiga = null;
                try
            {
                string getId = "select ime, autor, godina_izdanja,clan_id from knjiga where id=" + id;
                SqlCommand cmd = new SqlCommand(getId, conn);
                SqlDataReader rdr = cmd.ExecuteReader();
                if (rdr.Read())
                {
                    string ime = (string)rdr["ime"];
                    string autor = (string)rdr["autor"];
                    int godina_izdanja = (int)rdr["godina_izdanja"];
                    int clan_id = (int)rdr["clan_id"];

                    Clan clan = ClanUI.PronadjiClanaPoId(clan_id);
                    if (clan != null)
                    {
                        knjiga = new Knjiga(id, ime, autor, godina_izdanja, clan);
                    }
                    else
                        knjiga = new Knjiga(id,ime,  autor, godina_izdanja);

                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString()) ;
            }
            return knjiga;
        }
        public static bool PutNewBook(SqlConnection conn, Knjiga knjiga)
        {
            bool retVal = false;
            try
            {
                string dodaj = "insert into knjiga(ime, autor, godina_izdanja, clan_id) values(@ime, @autor, @godina,@id)";
                SqlCommand cmd = new SqlCommand(dodaj, conn);
                cmd.Parameters.AddWithValue("@ime" ,knjiga.Ime);
                cmd.Parameters.AddWithValue("@autor", knjiga.Autor);
                cmd.Parameters.AddWithValue("@godina", knjiga.Godina);
                cmd.Parameters.AddWithValue("@id", knjiga.Clan.Id);
                if (cmd.ExecuteNonQuery() == 1)
                {
                    retVal = true;
                }
            }
            catch (Exception e)
            {

                Console.WriteLine(e.ToString()); ;
            }
            return retVal;

        }
        public static List<Knjiga> GetAll(SqlConnection conn)
        {
            List<Knjiga> retVal = new List<Knjiga>();
            

            try
            {
                string all = "select id, ime, autor, godina_izdanja, clan_id from knjiga ";
                SqlCommand cmd = new SqlCommand(all, conn);
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    int id=(int)rdr["id"];
                    string ime = (string)rdr["ime"];
                    string autor = (string)rdr["autor"];
                    int godina_izdanja = (int)rdr["godina_izdanja"];
                    int clan_id = (int)rdr["clan_id"];

                    Knjiga knjiga = new Knjiga(id, ime, autor, godina_izdanja);
                    retVal.Add(knjiga);
                }
                rdr.Close();
            }
            catch (Exception e)
            {

                Console.WriteLine(e.ToString()); ;
            }
            return retVal;

        }
    }
}
